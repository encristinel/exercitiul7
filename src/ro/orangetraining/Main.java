package ro.orangetraining;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import static java.lang.System.arraycopy;

public class Main {

    public static void main(String[] args) {
        String[] receptionist = new String[5];
        Integer[] floor = new Integer[5];

        receptionist[0] = "John Snow";
        receptionist[1] = "William Cooper";
        receptionist[2] = "Patricia Johnson";
        receptionist[3] = "Kate Anderson";
        receptionist[4] = "Dustin Rhodes";


        floor[0] = 1;
        floor[1] = 2;
        floor[2] = 3;
        floor[3] = 4;
        floor[4] = 5;

        System.out.println("The receptionist on the " + floor[0] + "-st floor is: " + receptionist[0]);
        System.out.println("The receptionist on the " + floor[1] + "-st floor is: " + receptionist[1]);
        System.out.println("The receptionist on the " + floor[2] + "-st floor is: " + receptionist[2]);
        System.out.println("The receptionist on the " + floor[3] + "-st floor is: " + receptionist[3]);
        System.out.println("The receptionist on the " + floor[4] + "-st floor is: " + receptionist[4]);

        String[][] owner = new String[2][2];
        owner[0][0] = "domnul";
        owner[0][1] = "nume_0_1";
        owner[1][0] = "doamna";
        owner[1][1] = "nume_1_1";


        String[][] hotel = new String[2][2];
        hotel[0][0] = "nume hotel_0_0";
        hotel[0][1] = "nume hotel 0 1";
        hotel[1][0] = "nume hotel 1 0";
        hotel[1][1] = "nume hotel 1 1";

        System.out.println(owner[0][0] + " " + owner[0][1] + " and " + owner[1][0] + " " + owner[1][1] + " are the owners of the " + hotel[0][0]);

        char[] caractere = new char[]{'B', 'E', 'D', 'O', 'N', 'A', 'L', 'D', 'A', 'T', 'E'};
        char[] destinatie = new char[6];
        System.arraycopy(caractere, 2, destinatie, 0, 6);
        String new_dest=new String(destinatie); //de transformat din char in String

        System.out.println("The hotel's security is provided by: " +new_dest);
    }
    }

